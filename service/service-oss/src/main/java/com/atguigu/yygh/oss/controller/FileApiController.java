package com.atguigu.yygh.oss.controller;


import com.atguigu.yygh.common.result.Result;
import com.atguigu.yygh.oss.service.FileService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: FileApiController
 * Author:   swust-liuchuan
 * Date:     2022/7/21 22:26
 * Description: 文件上传
 * History:
 */

@Api(tags = "阿里云oss文件上传")
@RestController
@RequestMapping("/api/oss/file")
public class FileApiController {

    @Autowired
    private FileService fileService;

    /**
     * 上传文件到阿里云oss
     *
     * @param file
     * @return
     */
    @PostMapping("/fileUpload")
    public Result fileUpload(MultipartFile file) {
        //获取上传的文件
        String url = fileService.upload(file);
        return Result.ok(url);
    }
}