package com.atgui.easyexcel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: UserData
 * Author:   swust-liuchuan
 * Date:     2022/7/4 15:23
 * Description:
 * History:
 */

@Data
public class UserData {

    @ExcelProperty(value = "用户编号", index = 0)
    private int uid;

    @ExcelProperty(value = "用户名称", index = 1)
    private String username;
}