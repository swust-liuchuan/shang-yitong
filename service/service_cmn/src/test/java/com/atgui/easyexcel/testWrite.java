package com.atgui.easyexcel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: testWrite
 * Author:   swust-liuchuan
 * Date:     2022/7/4 15:25
 * Description:
 * History:
 */

public class testWrite {
    public static void main(String[] args) {
        //手动构建list集合
        List<UserData> list = new ArrayList<UserData>();

        for (int i = 0; i < 10; i++) {
            UserData data = new UserData();
            data.setUid(i);
            data.setUsername("测试" + i);
            list.add(data);
        }

        //设置excel文件路径和文件名称
        String fileName = "D:\\Program Files (x86)\\Java\\01.xlsx";

        //调用方法实现写入操作
        EasyExcel.write(fileName, UserData.class).sheet("用户信息表")
                .doWrite(list);
    }
}