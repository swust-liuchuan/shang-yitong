package com.atgui.easyexcel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: ExcelListener
 * Author:   swust-liuchuan
 * Date:     2022/7/5 22:00
 * Description:
 * History:
 */

public class ExcelListener extends AnalysisEventListener<UserData> {


    //一行一行的读取excel读取（从第二行开始读取，表头不读取）
    @Override
    public void invoke(UserData userData, AnalysisContext analysisContext) {
        System.out.println(userData);
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        String string = headMap.toString();
        System.out.println("表头信息" + string);
    }

    //读取之后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}