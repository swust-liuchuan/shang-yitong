package com.atgui.easyexcel;

import com.alibaba.excel.EasyExcel;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: testRead
 * Author:   swust-liuchuan
 * Date:     2022/7/5 21:57
 * Description:
 * History:
 */

public class testRead {
    public static void main(String[] args) {
        //设置excel文件路径和文件名称
        String fileName = "D:\\Program Files (x86)\\Java\\01.xlsx";
        //调用方法实现读取操作
        EasyExcel.read(fileName, UserData.class, new ExcelListener()).sheet().doRead();
    }
}