package com.atguigu.yygh.cmn.controller;

import com.atguigu.yygh.cmn.service.DictService;
import com.atguigu.yygh.common.result.Result;
import com.atguigu.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: DictController
 * Author:   swust-liuchuan
 * Date:     2022/7/3 23:46
 * Description:
 * History:
 */

@Slf4j
@Api(tags = "数据字典接口")
@RestController
@RequestMapping("/admin/cmn/dict")
public class DictController {

    @Autowired
    private DictService dictService;

    /**
     * 根据id查看数据词典信息，再根据parent_id查看是否有子节点,有则return
     *
     * @param id
     * @return
     */
    //根据数据id查询子数据列表
    @ApiOperation(value = "根据数据id查询子数据列表")
    @GetMapping("/findChildData/{id}")
    public Result findChildData(@PathVariable Long id) {
        List<Dict> list = dictService.findChildData(id);
        return Result.ok(list);
    }

    /**
     * 设置响应头格式，并执行EasyExcel的导出操作
     *
     * @param response
     * @return
     */
    //导出数据词典接口
    @ApiOperation(value = "下载数据词典")
    @GetMapping("/exportDict")
    public Result exportDict(HttpServletResponse response) {
        dictService.exportDictData(response);
        return Result.ok();
    }

    /**
     * 上传数据词典
     *
     * @param file
     * @return
     */
    //导入数据词典接口
    @ApiOperation(value = "上传入数据词典")
    @PostMapping("/uploadDict")
    public Result uploadDict(MultipartFile file) {
        dictService.uploadData(file);
        return Result.ok();
    }

    /**
     * 根据dictCode和value进行查询
     *
     * @param dictCode
     * @param value
     * @return
     */
    //根据dictCode和value进行查询
    @ApiOperation(value = "根据dictCode和value进行查询数据词典名称")
    @GetMapping("/getName/{dictCode}/{value}")
    public String getName(@PathVariable String dictCode, @PathVariable String value) {
        String dictName = dictService.getNameByDictCodeAndValue(dictCode, value);
        return dictName;
    }

    /**
     * 根据value查询
     *
     * @param value
     * @return
     */
    //根据value查询
    @ApiOperation(value = "根据value查询数据词典名称")
    @GetMapping("/getName/{value}")
    public String getName(@PathVariable String value) {
        String dictName = dictService.getNameByValue(value);
        return dictName;
    }

    /**
     * 根据dictCode查询id，再根据id查询子数据
     *
     * @param dictCode
     * @return
     */
    @ApiOperation(value = "根据dictCode查询到id，再根据id查询子数据")
    @GetMapping("/findByDictCode/{dictCode}")
    public Result findByDictCode(@PathVariable String dictCode) {
        List<Dict> list = dictService.findByDictCode(dictCode);
        return Result.ok(list);
    }
}