package com.atguigu.yygh.cmn.service;

import com.atguigu.yygh.model.cmn.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface DictService extends IService<Dict> {
    //根据数据id查询子数据列表
    @Cacheable(value = "dict", keyGenerator = "keyGenerator")
    List<Dict> findChildData(Long id);

    //导出数据词典
    void exportDictData(HttpServletResponse response);

    //上传数据
    @CacheEvict(value = "dict", allEntries = true)
    void uploadData(MultipartFile file);

    //根据dictCode和value查询或只根据value查询
    String getNameByDictCodeAndValue(String dictCode, String value);

    //根据value查询
    String getNameByValue(String value);

    //根据dictCode查询到id，再根据id查到parent_id
    List<Dict> findByDictCode(String dictCode);
}
