package com.atguigu.yygh.user.service.impl;

import com.atguigu.yygh.cmn.client.DictFeignClient;
import com.atguigu.yygh.enums.DictEnum;
import com.atguigu.yygh.model.user.Patient;
import com.atguigu.yygh.user.mapper.PatientMapper;
import com.atguigu.yygh.user.service.PatientService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: PatientServiceImpl
 * Author:   swust-liuchuan
 * Date:     2022/8/20 22:54
 * Description:
 * History:
 */

@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements PatientService {

    @Autowired
    private DictFeignClient dictFeignClient;

    //获取就诊人列表
    @Override
    public List<Patient> findAllUserId(Long userId) {
        //根据userid查询所有就诊人信息列表
        QueryWrapper<Patient> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<Patient> patientList = baseMapper.selectList(wrapper);
        //通过远程调用，得到编码对应具体内容，查询数据字典表内容
        patientList.stream().forEach(item -> {
            //其他参数封装
            this.packPatient(item);
        });
        return patientList;
    }

    @Override
    public Patient getPatientId(Long id) {
        return this.packPatient(baseMapper.selectById(id));
    }

    //Patient对象里面其他参数封装
    private Patient packPatient(Patient patient) {
        //TODO certificatesTypeString  && contactsCertificatesTypeString null值BUG待修复，前端暂时不影响
        //根据证件类型编码，获取证件类型具体指
        String certificatesTypeString =
                //使用枚举类获取证件类型的具体值
                dictFeignClient.getName(patient.getCertificatesType());//联系人证件
//        //联系人证件类型
//        String contactsCertificatesTypeString =
//                //同上同理
//                dictFeignClient.getName(patient.getContactsCertificatesType());

        //省
        String provinceString = dictFeignClient.getName(patient.getProvinceCode());
        //市
        String cityString = dictFeignClient.getName(patient.getCityCode());
        //区
        String districtString = dictFeignClient.getName(patient.getDistrictCode());

        //TODO null值BUG待修复，前端暂时不影响
        patient.getParam().put("certificatesTypeString", certificatesTypeString);
//        patient.getParam().put("contactsCertificatesTypeString", contactsCertificatesTypeString);

        patient.getParam().put("provinceString", provinceString);
        patient.getParam().put("cityString", cityString);
        patient.getParam().put("districtString", districtString);
        patient.getParam().put("fullAddress", provinceString + cityString + districtString + patient.getAddress());
        return patient;
    }
}
