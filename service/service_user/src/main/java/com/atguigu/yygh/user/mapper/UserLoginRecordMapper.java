package com.atguigu.yygh.user.mapper;

import com.atguigu.yygh.model.user.UserLoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserLoginRecordMapper extends BaseMapper<UserLoginRecord> {
}
