package com.atguigu.yygh.user.service.impl;

import com.atguigu.yygh.common.exception.YyghException;
import com.atguigu.yygh.common.helper.JwtHelper;
import com.atguigu.yygh.common.result.ResultCodeEnum;
import com.atguigu.yygh.enums.AuthStatusEnum;
import com.atguigu.yygh.model.user.Patient;
import com.atguigu.yygh.model.user.UserInfo;
import com.atguigu.yygh.model.user.UserLoginRecord;
import com.atguigu.yygh.user.mapper.UserInfoMapper;
import com.atguigu.yygh.user.service.PatientService;
import com.atguigu.yygh.user.service.UserInfoService;
import com.atguigu.yygh.user.service.UserLoginRecordService;
import com.atguigu.yygh.user.utils.IpUtil;
import com.atguigu.yygh.vo.user.LoginVo;
import com.atguigu.yygh.vo.user.UserAuthVo;
import com.atguigu.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: UserInfoServiceImpl
 * Author:   swust-liuchuan
 * Date:     2022/7/18 13:33
 * Description:
 * History:
 */

@Slf4j
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private UserLoginRecordService userLoginRecordService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private PatientService patientService;

    @Override
    public Map<String, Object> login(LoginVo loginVo, HttpServletRequest request) {

        String phone = loginVo.getPhone();
        String code = loginVo.getCode();
        String openid = loginVo.getOpenid();

        //登录成功后，返回登录信息name，token
        HashMap<String, Object> map = new HashMap<>();
        UserInfo userInfo = new UserInfo();

        //记录ip和user_id
        UserLoginRecord userLoginRecord = new UserLoginRecord();

        try {
            //openid无值，说明使用短信验证码登录
            if (openid == null || openid.equals("")) {
                //校验是否为空
                if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(code)) {
                    throw new YyghException(ResultCodeEnum.PARAM_ERROR);
                }
                //判断手机验证码与输入验证码是否一致
                String codeRedis = redisTemplate.opsForValue().get(phone);
                if (!code.equals(codeRedis)) {
                    throw new YyghException(ResultCodeEnum.CODE_ERROR);
                }

                //判断是否是第一次登录，是则新建用户
                userInfo = baseMapper.selectOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getPhone, phone));
                if (userInfo == null) {
                    userInfo = new UserInfo();
                    userInfo.setName("");
                    userInfo.setPhone(phone);
                    userInfo.setStatus(1);
                    //将新用户保存
                    baseMapper.insert(userInfo);
                }
            }

            //openid有值，说明使用微信扫码登录，微信扫码直接登录，不管是否绑定手机号
            if (openid != null && !openid.equals("")) {
                //通过这个openid得到userInfo实体信息
                userInfo = this.selectWxInfoOpenId(openid);
            }

            if (userInfo.getPhone() == null || userInfo.getPhone().equals("")) {//手机号为空，未绑定手机号
                //绑定手机号码
                userInfo.setPhone(phone);
                //更新到数据库
                boolean update = userInfoService.updateById(userInfo);
                log.info(update ? "更新登录信息成功" : "更新登录信息失败");
            } else if (!userInfo.getPhone().equals("") &&
                    userInfo.getPhone() != null &&
                    userInfo.getStatus() == 1) {
                //phone和手机号都不为空
                //登录成功后，返回登录信息name，token
                map = new HashMap<>();
                String name = userInfo.getName();
                //如果姓名为空，昵称作为姓名
                if (StringUtils.isEmpty(name)) {
                    name = userInfo.getNickName();
                }
                //如果昵称为空，手机号作为姓名
                if (StringUtils.isEmpty(name)) {
                    name = userInfo.getPhone();
                }
                map.put("name", name);

                //token生成
                String token = JwtHelper.createToken(userInfo.getId(), name);
                map.put("token", token);

                //已存在直接登录
                return map;
            }

            //使用手机号验证码登录，和第一次登录都要走这步
            //校验是否激活
            if (userInfo.getStatus() == 0) {
                throw new YyghException(ResultCodeEnum.LOGIN_DISABLED_ERROR);
            }


            String name = userInfo.getName();
            //如果姓名为空，昵称作为姓名
            if (StringUtils.isEmpty(name)) {
                name = userInfo.getNickName();
            }
            //如果昵称为空，手机号作为姓名
            if (StringUtils.isEmpty(name)) {
                name = userInfo.getPhone();
            }
            map.put("name", name);

            //token生成
            String token = JwtHelper.createToken(userInfo.getId(), name);
            map.put("token", token);
        } catch (Exception e) {
            e.printStackTrace();
            throw new YyghException(ResultCodeEnum.SERVICE_ERROR);
        } finally {
            userLoginRecord.setUserId(userInfo.getId());
            userLoginRecord.setIp(IpUtil.getIpAddr(request));
            boolean save = userLoginRecordService.save(userLoginRecord);
            log.info(save ? "保存登录信息成功" : "保存登录信息失败");
            log.info("用户信息" + userInfo);
        }

        //此时返回的是用手机号登录，和第一次微信扫码登录的结果
        return map;

    }

    @Override
    public UserInfo selectWxInfoOpenId(String openid) {
        UserInfo userInfo = baseMapper.selectOne(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getOpenid, openid));
        return userInfo;
    }

    //用户认证
    @Override
    public void userAuth(Long userId, UserAuthVo userAuthVo) {
        //根据用户id查询用户信息
        UserInfo userInfo = baseMapper.selectById(userId);
        //设置认证信息
        //认证人姓名
        userInfo.setName(userAuthVo.getName());
        //其他认证信息
        userInfo.setCertificatesType(userAuthVo.getCertificatesType());
        userInfo.setCertificatesNo(userAuthVo.getCertificatesNo());
        userInfo.setCertificatesUrl(userAuthVo.getCertificatesUrl());
        userInfo.setAuthStatus(AuthStatusEnum.AUTH_RUN.getStatus());
        //进行信息更新
        baseMapper.updateById(userInfo);
    }
    @Override
    public IPage<UserInfo> selectPage(Page<UserInfo> pageParam, UserInfoQueryVo userInfoQueryVo) {
        //UserInfoQueryVo获取条件值
        String name = userInfoQueryVo.getKeyword(); //用户名称
        Integer status = userInfoQueryVo.getStatus();//用户状态
        Integer authStatus = userInfoQueryVo.getAuthStatus(); //认证状态
        String createTimeBegin = userInfoQueryVo.getCreateTimeBegin(); //开始时间
        String createTimeEnd = userInfoQueryVo.getCreateTimeEnd(); //结束时间

        //构造lambda表达式
        LambdaQueryWrapper<UserInfo> lambdaQueryWrapper = new LambdaQueryWrapper<UserInfo>()
                .like(!StringUtils.isEmpty(name), UserInfo::getName, name)
                .eq(!StringUtils.isEmpty(status), UserInfo::getStatus, status)
                .eq(!StringUtils.isEmpty(authStatus), UserInfo::getAuthStatus, authStatus)
                .ge(!StringUtils.isEmpty(createTimeBegin), UserInfo::getCreateTime, createTimeBegin)
                .le(!StringUtils.isEmpty(createTimeBegin), UserInfo::getCreateTime, createTimeEnd);

        //调用mapper方法
        Page<UserInfo> userInfoPage = baseMapper.selectPage(pageParam, lambdaQueryWrapper);
        //封装
        userInfoPage.getRecords().stream().map(e -> {
            UserInfo userInfo = this.packageUserInfo(e);
            return userInfo;
        }).collect(Collectors.toList());

        return userInfoPage;
    }

    @Override
    public void lock(Long userId, Integer status) {
        UserInfo userInfo = userInfoService.getById(userId);
        userInfo.setStatus(status);
        userInfoService.updateById(userInfo);
    }

    @Override
    public Map<String, Object> show(Long userId) {
        HashMap<String, Object> map = new HashMap<>();
        UserInfo userInfo = this.packageUserInfo(baseMapper.selectById(userId));
        map.put("userInfo", userInfo);
        List<Patient> patientList = patientService.findAllUserId(userId);
        map.put("patientList", patientList);
        return map;
    }

    @Override
    public void approval(Long userId, Integer authStatus) {
        UserInfo userInfo = baseMapper.selectById(userId);
        userInfo.setAuthStatus(authStatus);
        baseMapper.updateById(userInfo);
    }

    //编号变成对应值封装
    private UserInfo packageUserInfo(UserInfo userInfo) {
        //处理认证状态编码
        userInfo.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(userInfo.getAuthStatus()));
        //处理用户状态 0  1
        String statusString = userInfo.getStatus().intValue() == 0 ? "锁定" : "正常";
        userInfo.getParam().put("statusString", statusString);
        return userInfo;
    }
}