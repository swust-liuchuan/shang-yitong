package com.atguigu.yygh.user.controller;

import com.atguigu.yygh.common.utils.AuthContextHolder;
import com.atguigu.yygh.common.result.Result;
import com.atguigu.yygh.model.user.UserInfo;
import com.atguigu.yygh.user.service.UserInfoService;
import com.atguigu.yygh.user.utils.IpUtil;
import com.atguigu.yygh.vo.user.LoginVo;
import com.atguigu.yygh.vo.user.UserAuthVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: UserInfoApiController
 * Author:   swust-liuchuan
 * Date:     2022/7/18 13:44
 * Description:
 * History:
 */

@Slf4j
@Api(tags = "前台用户登录")
@RestController
@RequestMapping("/api/user")
public class UserInfoApiController {

    @Autowired
    private UserInfoService userInfoService;


    /**
     * 用户登录
     *
     * @param loginVo
     * @param request
     * @return
     */
    @ApiOperation(value = "用户登录")
    @PostMapping("/login")
    public Result login(@RequestBody LoginVo loginVo, HttpServletRequest request) {
        String ipAddr = IpUtil.getIpAddr(request);
        loginVo.setIp(ipAddr);
        Map<String, Object> userInfo = userInfoService.login(loginVo, request);
        return Result.ok(userInfo);
    }

    /**
     * 用户认证
     *
     * @param userAuthVo
     * @param request
     * @return
     */
    @ApiOperation(value = "用户认证")
    @PostMapping("auth/userAuth")
    public Result userAuth(@RequestBody UserAuthVo userAuthVo, HttpServletRequest request) {
        //传递两个参数，第一个参数用户id，第二个参数认证数据vo对象
        userInfoService.userAuth(AuthContextHolder.getUserId(request),userAuthVo);
        return Result.ok();
    }
    /**
     * 获取当前用户信息
     *
     * @param request
     * @return
     */
    @ApiOperation(value = "获取当前用户信息")
    @GetMapping("auth/getUserInfo")
    public Result getUserInfo(HttpServletRequest request) {
        Long userId = AuthContextHolder.getUserId(request);
        UserInfo userInfo = userInfoService.getById(userId);
        return Result.ok(userInfo);
    }
}