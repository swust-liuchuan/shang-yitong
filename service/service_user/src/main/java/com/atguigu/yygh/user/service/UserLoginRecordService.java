package com.atguigu.yygh.user.service;

import com.atguigu.yygh.model.user.UserLoginRecord;
import com.baomidou.mybatisplus.extension.service.IService;

public interface UserLoginRecordService extends IService<UserLoginRecord> {
}
