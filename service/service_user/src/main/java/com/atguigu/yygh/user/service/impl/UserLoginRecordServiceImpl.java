package com.atguigu.yygh.user.service.impl;

import com.atguigu.yygh.model.user.UserLoginRecord;
import com.atguigu.yygh.user.mapper.UserLoginRecordMapper;
import com.atguigu.yygh.user.service.UserLoginRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: UserLoginRecordServiceImpl
 * Author:   swust-liuchuan
 * Date:     2022/7/22 2:27
 * Description:
 * History:
 */

@Service
public class UserLoginRecordServiceImpl extends ServiceImpl<UserLoginRecordMapper, UserLoginRecord> implements UserLoginRecordService {

}