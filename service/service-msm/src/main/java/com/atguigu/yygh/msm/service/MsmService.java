package com.atguigu.yygh.msm.service;

import com.atguigu.yygh.vo.msm.MsmVo;

public interface MsmService {
    /**
     * 发送验证码
     *
     * @param phone
     * @param code
     * @return
     */
    boolean sendCode(String phone, String code);

    boolean send(MsmVo msmVo);
}
