package com.atgui.yygh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Copyright (C), 2022-2022, 西南科技大学
 * FileName: ServerGatewayApplication
 * Author:   swust-liuchuan
 * Date:     2022/7/16 20:39
 * Description:
 * History:
 */
@SpringBootApplication
public class ServerGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServerGatewayApplication.class, args);
    }
}